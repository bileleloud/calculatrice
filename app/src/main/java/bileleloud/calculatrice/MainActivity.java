package bileleloud.calculatrice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;



/**
 * Class Main.
 */
public class MainActivity extends AppCompatActivity {
    /**
     * variable a .
     */
    private EditText a;
    /**
     * variable  b.
     */
    private EditText b;
    @Override
    protected final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        a = (EditText) findViewById(R.id.a);
        b = (EditText) findViewById(R.id.b);
    }
    /**
     * @param view
     * fkfj.
     */
    public final void add(final View view) {
        double n1;
        double n2;
        double res;
        n1 = Double.parseDouble(a.getText().toString());
        n2 = Double.parseDouble(b.getText().toString());
        res = n1 + n2;
        Toast.makeText(this, "la Résultat : " + res, Toast.LENGTH_LONG).show();
    }

    /**
     * @param view
     * methode pour clear l'interface.
     */
    public final void clear(final View view) {
        a.setText("");
        b.setText("");
    }

}
